package com.onikibir;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;


import com.onikibir.R;

import custom_font.MyEditText;
import custom_font.MyTextView;

public class RegisterActivity extends AppCompatActivity {
    TextView holliday;
    MyTextView signin;
    MyTextView register;

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);

        signin = (MyTextView) findViewById(R.id.signin);
        register = (MyTextView) findViewById(R.id.register);

        holliday = (TextView)findViewById(R.id.holliday);


        final MyEditText userMail = (MyEditText) findViewById(R.id.email);
        final MyEditText userPass = (MyEditText) findViewById(R.id.password);
        final MyEditText userPass2 = (MyEditText) findViewById(R.id.repassword);
        final TextView info = (TextView) findViewById(R.id.info);


        Typeface custom_fonts = Typeface.createFromAsset(getAssets(), "fonts/ArgonPERSONAL-Regular.otf");
        holliday.setTypeface(custom_fonts);


        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(RegisterActivity.this, MainActivity.class);
                startActivity(it);
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = "Name";
                String lastname = "Lastname";
                String phone = "phone";
                String mail = userMail.getText().toString();
                String password = userPass.getText().toString();
                String password2 = userPass2.getText().toString();
                Database vt = new Database(RegisterActivity.this);

                if(vt.checkMail(mail)){
                    info.setText("Bu Eposta kullanılıyor,farklı bir eposta deneyin");
                } else if (mail.isEmpty() || password.isEmpty() || password2.isEmpty()) {
                    info.setText("Lütfen alanları bos bırakmayın");
                }else if(!isValidEmailAddress(mail)){
                    info.setText("Lütfen dogru bir mail adresi giriniz");
                }else if(!password.equals(password2)){
                    info.setText("Lütfen aynı sifre giriniz");
                }else{
                    vt.addData(name, lastname, phone,mail,password);

                    Log.d("Click","Register Yes");
                    Intent it = new Intent(RegisterActivity.this, MainActivity.class);
                    it.putExtra("comfirm","Kayıt Başarılı");
                    startActivity(it);}


            }

        });

    }
}
