package com.onikibir;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.onikibir.BeanClassForListView;
import com.onikibir.R;

import java.util.ArrayList;

public class FirstFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    private int mPageNo;
    TextView incele;

    public String CITY = "Muğla";
    public int[] IMAGE = {R.drawable.halisaha1, R.drawable.halisaha2, R.drawable.halisaha3, R.drawable.halisaha1, R.drawable.halisaha2, R.drawable.halisaha3, R.drawable.halisaha1, R.drawable.halisaha2};
    public String[] AWESOM = {"Saray Halısaha","Ula Halısaha","Kötekli Halısaha","Saray Halısaha","Ula Halısaha","Kötekli Halısaha" ,"Ula Halısaha","Kötekli Halısaha"};
    public String[] MONEY = {"₺120 / Saat", "₺100 / Saat","₺110 / Saat", "₺120 / Saat", "₺100 / Saat","₺110 / Saat","₺110 / Saat","₺110 / Saat"};
    public String[] NUMBER = {"3.3", "3", "4.5", "4.0", "3.5", "4.5","3.0","4.0"};


    private ArrayList<BeanClassForListView> beanClassArrayList;
    private listViewAdapter listViewAdapter;


    public FirstFragment() {


    }

    public static FirstFragment newInstance(int pageNo) {

        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNo);
        FirstFragment fragment = new FirstFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNo = getArguments().getInt(ARG_PAGE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_list, container, false);
//        TextView textView = (TextView) view;
//        textView.setText("Fragment #" + mPageNo);

        incele = (TextView) view.findViewById(R.id.incele);





        final ListView listview = (ListView) view.findViewById(R.id.listview);
        beanClassArrayList = new ArrayList<BeanClassForListView>();
        TextView city = (TextView) view.findViewById(R.id.city);
        city.setText(CITY);


        for (int i = 0; i < IMAGE.length; i++) {

            BeanClassForListView beanClass = new BeanClassForListView(IMAGE[i], AWESOM[i], MONEY[i],NUMBER[i],CITY);
            beanClassArrayList.add(beanClass);

        }
        listViewAdapter = new listViewAdapter(getActivity(), beanClassArrayList);
        listview.setAdapter(listViewAdapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Object o =  listview.getItemAtPosition(position);
                final BeanClassForListView bean  = (BeanClassForListView) o ;

                incele = (TextView) view.findViewById(R.id.incele);
                TextView rezervasyon = (TextView) view.findViewById(R.id.rezervasyon);
                incele.setOnClickListener(new View.OnClickListener() {
                    @Override


                    public void onClick(View view) {
                        Log.d("Durum2","İncele"+view.getId()+"id"+R.id.incele);
                        Fragment fragment = null;
                        switch (view.getId()) {
                            case R.id.incele:
                                fragment=DetailFragment.newInstance(bean);
                                replaceFragment(fragment);
                                break;




                        }
                    }
                });

                rezervasyon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Fragment fragment = null;
                        switch (v.getId()) {
                            case R.id.rezervasyon:
                                fragment=RezFragment.newInstance(bean);
                                replaceFragment(fragment);




                        }

                    }
                });




                Log.d("Durum","TıklandıList"+bean.getCity()+"id="+view.getId()+"");


             }
        });
//        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
//
//                Fragment fragment = null;
//                switch (v.getId()) {
//                    case R.id.incele:
//                        fragment = new PaymentFragment();
//                        replaceFragment(fragment);
//                        break;
//
//
//                }
//            }
//        });

        return view;
    }





    public void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.listallitems, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();



    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


}
