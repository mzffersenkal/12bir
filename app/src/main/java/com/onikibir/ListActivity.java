package com.onikibir;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.onikibir.BeanClassForListView;
import com.onikibir.R;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {


    public String CITY = "Muğla";
    public int[] IMAGE = {R.drawable.home1, R.drawable.home2, R.drawable.home3, R.drawable.home4, R.drawable.home5, R.drawable.home1, R.drawable.home2, R.drawable.home3};
    public String[] AWESOM = {"Awesom Villa","Awesom Villa","Awesom Villa","Awesom Villa","Awesom Villa","Awesom Villa","Awesom Villa","Awesom Villa"};
    public String[] RATE = {"\u20B9 3200 / Night", "\u20B9 3200 / Night","\u20B9 3200 / Night", "\u20B9 3200 / Night", "\u20B9 3200 / Night", "\u20B9 3200 / Night","\u20B9 3200 / Night","\u20B9 3200 / Night"};
    public String[] NUMBER = {"3.3", "3", "4.5", "4.0", "3.5", "4.5","3.0","4.0"};


    private ArrayList<BeanClassForListView> beanClassArrayList;
    private listViewAdapter listViewAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);


        ListView listview = (ListView) findViewById(R.id.listview);
        beanClassArrayList = new ArrayList<BeanClassForListView>();
        TextView city = (TextView) findViewById(R.id.city);
        city.setText(CITY);


        for (int i = 0; i < IMAGE.length; i++) {

            BeanClassForListView beanClass = new BeanClassForListView(IMAGE[i], AWESOM[i], RATE[i],NUMBER[i],CITY);
            beanClassArrayList.add(beanClass);

        }
        listViewAdapter = new listViewAdapter(ListActivity.this, beanClassArrayList);
        listview.setAdapter(listViewAdapter);



    }
}
