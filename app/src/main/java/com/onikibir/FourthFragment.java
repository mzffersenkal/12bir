package com.onikibir;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FourthFragment extends Fragment implements View.OnClickListener  {
    public static final String ARG_PAGE = "ARG_PAGE";
    private int mPageNo;
    TextView listele;

    public FourthFragment() {
        // Required empty public constructor
    }

    public static FourthFragment newInstance(int pageNo) {

        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNo);
        FourthFragment fragment = new FourthFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNo = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fourth_fragment, container, false);
//        TextView textView = (TextView) view;
//        textView.setText("Fragment #" + mPageNo);

        listele = (TextView) view.findViewById(R.id.listele);
        listele.setOnClickListener(this);

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    @Override
    public void onClick(View v) {
        Log.d("Durum","id="+v.getId()+"id="+R.id.listele+"");
        Fragment fragment = null;
        switch (v.getId()) {
            case R.id.listele:
                fragment = FirstFragment.newInstance(1);

                replaceFragment(fragment);
                break;


        }
    }

    public void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.activity_list, someFragment);

        transaction.addToBackStack(null);
        transaction.commit();
    }
}
