package com.onikibir;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SecondFragment extends Fragment {

    public static final String ARG_PAGE = "ARG_PAGE";
    private static final String FORMAT = "%02d:%02d:%02d";


    private int mPageNo;

    public SecondFragment() {
        // Required empty public constructor
    }

    public static SecondFragment newInstance(int pageNo) {

        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNo);
        SecondFragment fragment = new SecondFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNo = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view =  inflater.inflate(R.layout.second_fragment, container, false);
        final TextView_Lato text1 = (TextView_Lato) view.findViewById(R.id.saat);
        final TextView_Lato text2 = (TextView_Lato) view.findViewById(R.id.dakika);
        final TextView_Lato text3 = (TextView_Lato) view.findViewById(R.id.saniye);

        new CountDownTimer(16069000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {

                int seconds = (int) (millisUntilFinished / 1000);

                int hours = seconds / (60 * 60);
                int tempMint = (seconds - (hours * 60 * 60));
                int minutes = tempMint / 60;
                seconds = tempMint - (minutes * 60);

                text1.setText(String.format("%02d", hours));
                text2.setText(String.format("%02d", minutes));
                text3.setText(String.format("%02d", seconds));
            }

            public void onFinish() {
                text1.setText("done!");
            }
        }.start();

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

}
