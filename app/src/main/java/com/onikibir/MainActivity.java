package com.onikibir;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.VideoView;

import custom_font.MyTextView;

public class MainActivity extends AppCompatActivity {
    TextView holliday;
    MyTextView create;
    MyTextView admin;
    MyTextView login;
    private VideoView mVideoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        mVideoView = (VideoView) findViewById(R.id.bgvideoview);
        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.bgvideo);

        Log.d("hata", "onCreate: ");
        mVideoView.setVideoURI(uri);
        mVideoView.start();

        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.setLooping(false);
            }
        });

        create = (MyTextView)findViewById(R.id.create);
        admin = (MyTextView)findViewById(R.id.admin);
        login = (MyTextView)findViewById(R.id.login);
        holliday = (TextView)findViewById(R.id.holliday);

        Typeface custom_fonts = Typeface.createFromAsset(getAssets(), "fonts/ArgonPERSONAL-Regular.otf");
        holliday.setTypeface(custom_fonts);

        final TextView comfirm = (TextView) findViewById(R.id.comfirm);
        Intent it = getIntent();
        String getConfirm = it.getStringExtra("comfirm");
        comfirm.setText(getConfirm);

        // User Check

        final EditText uyeMail = (EditText) findViewById(R.id.mail);
        final EditText uyePassword = (EditText) findViewById(R.id.password);
        final TextView info = (TextView) findViewById(R.id.info);
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String mail = uyeMail.getText().toString();
                String password = uyePassword.getText().toString();

                Log.e("Veri",mail);
                Log.e("Veri",password);

                Database vt = new Database(MainActivity.this);

                if(vt.checkUser(mail,password)){
                    Log.d("Check","Yes");
                    Intent myIntent = new Intent(v.getContext(), Home.class);
                    startActivityForResult(myIntent, 0);

                }else{
                    Log.d("Check","No");
                    info.setText("Lütfen bilgilerinizi kontrol ediniz.");


                }





            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent it = new Intent(MainActivity.this,RegisterActivity.class);
                startActivity(it);
            }
        });

        admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent it = new Intent(MainActivity.this,Home.class);
                startActivity(it);
            }
        });


    }
}
