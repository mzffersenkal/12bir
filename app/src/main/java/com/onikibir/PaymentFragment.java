package com.onikibir;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.onikibir.BeanClassForListView;

@SuppressLint("ValidFragment")
public class PaymentFragment extends Fragment implements  View.OnClickListener{

    ImageView backpage;
    BeanClassForListView bean;

    public PaymentFragment(BeanClassForListView bean) {
        // Required empty public constructor
        this.bean = bean;
    }

    public static PaymentFragment newInstance(BeanClassForListView bean) {

        Bundle args = new Bundle();

        PaymentFragment fragment = new PaymentFragment(bean);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.payment, container, false);
//        TextView textView = (TextView) view;
        TextView paynow = (TextView) view.findViewById(R.id.paynow);
        backpage = (ImageView) view.findViewById(R.id.backpage);
        paynow.setOnClickListener(this);
        backpage.setOnClickListener(this);
//        textView.setText("Fragment #" + mPageNo);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    @Override
    public void onClick(View v) {
        Fragment fragment = null;
        switch (v.getId()) {
            case R.id.paynow:
                fragment = new SuccessFragment();
                replaceFragment(fragment);
                break;

            case R.id.backpage:
                fragment = RezFragment.newInstance(bean);
                replaceFragment(fragment);
                break;


        }
    }
    public void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.activity_main, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
