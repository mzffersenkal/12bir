package com.onikibir;


public class BeanClassForListView {

    private int image;
    private  String awesom;
    private String money;
    private String number;
    private String city;


    public BeanClassForListView(int image, String awesom, String money, String number,String city) {
        this.image = image;
        this.awesom = awesom;
        this.money = money;
        this.number = number;
        this.city = city;
    }

    public BeanClassForListView(int i, String s, String s1) {

    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getAwesom() {
        return awesom;
    }
    public String getCity() {
        return city;
    }

    public void setAwesom(String awesom) {
        this.awesom = awesom;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }



}
