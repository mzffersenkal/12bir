package com.onikibir;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SuccessFragment extends Fragment implements View.OnClickListener {



    public SuccessFragment() {
        // Required empty public constructor
    }

    public static SuccessFragment newInstance(int pageNo) {

        Bundle args = new Bundle();

        SuccessFragment fragment = new SuccessFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.success, container, false);
//        TextView textView = (TextView) view;
        TextView backtohome = (TextView) view.findViewById(R.id.backtohome);
        backtohome.setOnClickListener(this);

//        textView.setText("Fragment #" + mPageNo);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    @Override
    public void onClick(View v) {
        Fragment fragment = null;
        switch (v.getId()) {
            case R.id.backtohome:
                fragment = FirstFragment.newInstance(1);
                replaceFragment(fragment);
                break;


        }
    }

    public void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.activity_main, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();



    }
}
