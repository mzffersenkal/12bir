package com.onikibir;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("ValidFragment")
public class DetailFragment extends Fragment implements View.OnClickListener {
    public static final String ARG_PAGE = "ARG_PAGE";
    private int mPageNo;
    TextView book;
    BeanClassForListView bean;



    public DetailFragment(BeanClassForListView bean) {
        // Required empty public constructor
        this.bean = bean;
    }

    public static DetailFragment newInstance(BeanClassForListView bean) {

        Bundle args = new Bundle();
     //   args.putInt(ARG_PAGE, pageNo);
        DetailFragment fragment = new DetailFragment(bean);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     //   mPageNo = getArguments().getInt(ARG_PAGE);
    }
    public void clickMethod(View v) {
        Log.d("Durum","Tıklandı");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_detail, container, false);
//        TextView textView = (TextView) view;
//        textView.setText("Fragment #" + mPageNo);
        TextView money = (TextView) view.findViewById(R.id.money);
        ImageView image = (ImageView) view.findViewById(R.id.detailImage);
        TextView rate = (TextView) view.findViewById(R.id.rate);
        TextView halisaha = (TextView) view.findViewById(R.id.halisahaName);
        TextView halisahacity = (TextView) view.findViewById(R.id.halisahaCity);



        money.setText(bean.getMoney());
        image.setImageResource(bean.getImage());
        rate.setText(bean.getNumber());
        halisaha.setText(bean.getAwesom());
        halisahacity.setText(bean.getCity());


        book = (TextView) view.findViewById(R.id.book);
        book.setOnClickListener(this);

        return view;
    }
    public void onClick(View v) {
        Log.d("Durum","Tıklandı");
        Fragment fragment = null;
        switch (v.getId()) {
            case R.id.book:
                fragment = RezFragment.newInstance(bean);
                replaceFragment(fragment);
                break;


        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);






    }

    public void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.activity_main, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }




    @Override
    public void onDetach() {
        super.onDetach();

    }


}
