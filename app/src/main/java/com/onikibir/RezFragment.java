package com.onikibir;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.onikibir.BeanClassForListView;

import java.util.Calendar;

@SuppressLint("ValidFragment")
public class RezFragment extends Fragment implements View.OnClickListener {
    ImageView backpage;
    TextView pay;
    BeanClassForListView bean;


    public RezFragment(BeanClassForListView bean) {
        // Required empty public constructor
        this.bean=bean;
    }

    public static RezFragment newInstance(BeanClassForListView bean) {

        Bundle args = new Bundle();

        RezFragment fragment = new RezFragment(bean);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mPageNo = getArguments().getInt(ARG_PAGE);
    }
    public void clickMethod(View v) {
        Log.d("Durum","Tıklandı");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_rezer, container, false);
//        TextView textView = (TextView) view;
//        textView.setText("Fragment #" + mPageNo);


        final EditText Edit_Time = (EditText) view.findViewById(R.id.time_view_edit);
        final EditText Edit_Time2 = (EditText) view.findViewById(R.id.time_view_edit2);
        Edit_Time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        Edit_Time.setText( selectedHour + ":" + "00");
                        Edit_Time2.setText( selectedHour+1 + ":" + "00");


                    }
                }, hour, minute, true);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

        pay = (TextView) view.findViewById(R.id.pay);
        backpage = (ImageView) view.findViewById(R.id.rezervgeri);
        ImageView rezervImage = (ImageView) view.findViewById(R.id.rezervImage);
        TextView miktar = (TextView) view.findViewById(R.id.miktar);


        rezervImage.setImageResource(bean.getImage());
        String[] parts = bean.getMoney().split("/");
        pay.setText("Öde "+parts[0]);
        miktar.setText(parts[0]);
        backpage.setOnClickListener(this);
        pay.setOnClickListener(this);


        return view;
    }
    public void onClick(View v) {
        Log.d("Durum","Tıklandı");
        Fragment fragment = null;
        switch (v.getId()) {
            case R.id.pay:
                fragment = PaymentFragment.newInstance(bean);
                replaceFragment(fragment);
                break;

            default:
                Log.d("Durum","Tıklandı2");
                fragment = DetailFragment.newInstance(bean);
                replaceFragment(fragment);
                break;





        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);






    }

    public void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.activity_main, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }




    @Override
    public void onDetach() {
        super.onDetach();

    }


}
